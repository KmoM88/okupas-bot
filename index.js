// Init the environment variables and server configurations
require("dotenv").config();

// create a file only file logger
const log = require('simple-node-logger').createSimpleFileLogger(opts = {
    logFilePath:'project.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
},);
log.info('Inicio de OkupasBot');

// Import the required packages
const twitController = require("./twitter-bot/twit_controller");
// const frases = require("./twitter-bot/frases");

// const video = require("./twitter-bot/video-stitch");

// twitController.retweetAnswerOn("@OkupasBot")

//Init tetweet object
twitController.readJSON(log);

// Get tweets by hashtag
let hashtagSearch = twitController.search(['argentina','inviable','ezeiza'],100);
hashtagSearch
    .then(r => {
        // console.log(r.statuses)
        r.statuses.forEach((tweet) => {
            // console.log(tweet)
            twitController.saveToJSON(tweet, log)
        });
    })
    .catch(err => console.log(err))

setInterval(() => {
    let hashtagReSearch = twitController.search(['argentina','inviable','ezeiza'],100);
    hashtagReSearch
    .then(r => {
        // console.log(r.statuses)
        r.statuses.forEach((tweet) => {
            // console.log(tweet)
            twitController.saveToJSON(tweet, log)
        });
    })
    .catch(err => console.log(err))
}, 1000*60*60);
setInterval(() => twitController.replyTweet(log), 1000*60)
//Periodic action 1
// periodo1 = 1000*60
// twitController.tweetTextFromList();
// setInterval(twitController.tweetTextFromList, periodo1)

//Periodic video 1
// let periodoVideo1 = 1000*60*30; //30min
// twitController.videoFromList(log);
// setInterval(() => twitController.videoFromList(log), periodoVideo1);
