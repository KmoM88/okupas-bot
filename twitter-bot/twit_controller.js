const client = require("./twit");
const videoPathArray = require("./video-list")
const input = require("./input");

const fs = require("fs");
const path = require('path');

const filePath = 'assets/';
const PATH = path.join(__dirname,filePath);
const path_anda_nomas = PATH + 'video\\cut\\02\\02-19-anda-maquina.mp4'
let retweet_list = {}

// async function tweetEvent(tweet) {
//   //   let file = "./input.txt";
//   // Who is this in reply to?
//   // console.log(tweet);
//   // What is the text?
//   let txt = tweet.text;
//   // Get rid of the @ mention
//   let twit_text = txt.replace(/@feder0ssi /g, "");
//   let file_text = "";
//   let replyText = "Ubicación " + tweet.user.screen_name + ": '" + tweet.user.location + "'. " + input.input_text + "? Creo que si!";
//   // const data = await twitController.replyTweet(replyText, tweet.user.is_str);
//   // console.log("Respondido a mention:" + data.text)
// }


const twitController = {
    // retweetAnswerOn: async (account) => {
    //   return true
    //   try {
    //   // Set up a user stream tracking mentions to username
    //   this.streamCreate(account);
    //   // Now looking for tweet events
    //   // await twitController.streamOn();
    //   } catch (err) {
    //     console.log(err);
    //   }
    // },
//   postTweet: async (text) => {
//     return new Promise((resolve, reject) => {
//       let params = {
//         status: text,
//       };
//       console.log(params);
//       client.post("statuses/update", params, (err2, data) => {
//         if (err2) {
//           return reject(err2);
//         } else {
//           return resolve(data);
//         }
//       });
//     });
//   },
//   postReTweet: async (id) => {
//     return new Promise((resolve, reject) => {
//       let params = {
//         id,
//       };
//       client.post("statuses/retweet/:id", params, (err, data) => {
//         if (err) {
//           return reject(err);
//         } else {
//           return resolve(data);
//         }
//       });
//     });
//   },


  search: async (hash, count) => {
    return new Promise((resolve, reject) => {
      let params = {
        q: hash,
        count
      };
      client.get("search/tweets", params, (error, data) => {
        if (error) {
          return reject(error);
        } else {
          return resolve(data);
        }
      });
    });
  },


  // tweetTextFromList: async () => {
  //   let tweet = "okupas";
  //   client.post('statuses/update', {status: tweet}).then((data) => {console.log(data);}).catch((err) => {console.log(err);})
  // },


  videoFromList: async (log) => {

    if (videoPathArray.length > 0) {
      const random = Math.floor(Math.random() * videoPathArray.length);
      client.postMediaChunked({file_path: videoPathArray[random]}, function (err, data, response) {
        const mediaIdStr = data.media_id_string;
        const meta_params = { media_id: mediaIdStr };
        client.post('media/metadata/create', meta_params, function (err, data, response) {
          if (!err) {
            const params = { status: '#okupas', media_ids: [mediaIdStr] };
            client.post('statuses/update', params, function (err, tweet, response) {
              // console.log(tweet);
              log.info(`Video ${videoPathArray[random]} posted with TwId:${tweet.id}`)
              // console.log(response);
            }); // end '/statuses/update'
          } // end if(!err)
        }); // end '/media/metadata/create'
      }); // end T.postMedisChunked
    }
    else {
      log.info('No se pudo obtener la lista de videos');
    }
  },

  // streamCreate: async (account) => {
  //   return new Promise(async (resolve, reject) => {
  //       let params = {
  //           track: account,
  //       };
  //       let stream = await client.stream("statuses/filter", params)
  //       await stream.on("tweet", tweetEvent)
  //   });
  // },
  // streamOn: async () => {
  //       try {
  //           stream.on("tweet", this.tweetEvent);
  //       } catch (error) {
  //           console.error(error)
  //       }
  // },
//   replyTweet: async (text, id) => {
//     return new Promise((resolve, reject) => {
//         let params = {
//           status: text,
//           in_reply_to_status_id: id,
//         };
//         client.post("statuses/update", params, (err2, data) => {
//           if (err2) {
//             return reject(err2);
//           } else {
//             return resolve(data);
//           }
//         });
//       });
//   }
  replyTweet: async (log) => {
    // console.log(retweet_list.respondido.findIndex(false))
    for (let i = 0; i < retweet_list.length; i++) {
      if (!retweet_list[i].respondido){
        // console.log(retweet_list[i])
        // console.log(retweet_list[i].id_str)
        retweet_list[i].respondido = true;
        // console.log(retweet_list[i].respondido)
        client.postMediaChunked({file_path: path_anda_nomas}, function (err, data, response) {
          const mediaIdStr = data.media_id_string;
          const meta_params = { media_id: mediaIdStr };
          client.post('media/metadata/create', meta_params, function (err, data, response) {
            if (!err) {
              const params = {
                status: `@${retweet_list[i].screen_name} Andá máquina nomás. Nadie te detiene. #okupas`,
                media_ids: [mediaIdStr],
                in_reply_to_status_id: retweet_list[i].id_str,
              };
              client.post('statuses/update', params, function (err, tweet, response) {
                // console.log(err);
                log.info(`Responded ${retweet_list[i].id_str}`)
                // console.log(response);
              }); // end '/statuses/update'
            } // end if(!err)
          }); // end '/media/metadata/create'
        }); // end T.postMedisChunked
        fs.writeFile(PATH + 'retweet_list.json', JSON.stringify(retweet_list), (err, data) => {
          // console.log(err)
        });
        return
      }
    }
  },

  readJSON: (log) => {
    fs.readFile(PATH + 'retweet_list.json', (err, data) => {
        retweet_list = JSON.parse(data);
    });
  },

  saveToJSON: (tweet, log) => {
      if (retweet_list.map(tw => tw.id_str).includes(tweet.id_str)) {
          log.info(`Tweet ID=${tweet.id_str} ya agregado`)
      }
      else{
          if(tweet.retweeted_status != undefined) {
            if(tweet.retweeted_status.id_str != undefined && tweet.retweeted_status.user.id_str != undefined) {
              if (retweet_list.map(tw => tw.id_str).includes(tweet.retweeted_status.id_str)) {
                log.info(`Tweet ID=${tweet.retweeted_status.id_str} ya agregado`)
              }
              else {
                retweet_list.push({
                    id_str: tweet.retweeted_status.id_str,
                    id_usr: tweet.retweeted_status.user.id_str,
                    name: tweet.retweeted_status.user.name,
                    screen_name: tweet.retweeted_status.user.screen_name,
                    respondido: false
                  })
                  log.info(`Agregado a lista tweetID= ${tweet.retweeted_status.id_str}`)
              }
          }}
          else {
            retweet_list.push({
              id_str: tweet.id_str,
              id_usr: tweet.user.id_str,
              name: tweet.user.name,
              screen_name: tweet.user.screen_name,
              respondido: false
            })
            log.info(`Agregado a lista tweetID= ${tweet.id_str}`)
          }
          fs.writeFile(PATH + 'retweet_list.json', JSON.stringify(retweet_list), (err, data) => {
              // console.log(err)
          });
      }
  }
}

module.exports = twitController;
