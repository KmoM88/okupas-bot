const fs = require("fs");
const path = require('path');

const filePath = 'assets/video/cut/';
const PATH = path.join(__dirname,filePath);
const videoPathArray = []
const videoPathObj = {
    '01': [],
    '02': [],
    '03': [],
    '04': [],
    '05': [],
    '06': [],
    '07': [],
    '08': [],
    '09': [],
    '10': [],
    '11': [],
}

// console.log('Obteniendo lista de archivos en: ' + PATH)

Object.keys(videoPathObj).forEach(folder => {
    fs.readdir(PATH + folder , (err, files) => {
    if (err) {
        return console.log('Unable to scan directory: ' + err);
        }
    files.forEach(function (file) {
        videoPathArray.push(PATH + folder + '/' + file);
        videoPathObj[folder].push(PATH + folder + '/' + file);
        });
    });
});

module.exports = videoPathArray;